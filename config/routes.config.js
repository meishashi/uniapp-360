/*
 * 路由表对象：
 * 该文件挂载在Vue原型中 $mRoutesConfig
 * 作用：调用$mRouter对象的方法 传入以下对应的路由对象，详细见common目录下的router.js
 * 示例：this.$mRouter.push({route:this.$mRoutesConfig.main,query:{a:1}})
 * 注意：所有在pages目录下新建的页面都必须在"路由表"中进行声明，并且在框架的pages.json注册。
 *
 * 配置参数项说明：
 * name:可选配置 （路由名称）
 * path:必填配置 （路由地址）
 * requiresAuth:可选配置 （是否权限路由）
 */

export default {
	// 权限路由 在main.js可实现路由拦截 所以路由都需要注册 待完善
	promoCode: {
		name: '创建订单',
		path: '/pages/order/create/order',
		requiresAuth: true
	},
	payment: {
		name: '还款页面',
		path: '/pages/payment/index',
		requiresAuth: true
	},
	auth: {
		name: '授权页面',
		path: '/pages/auth/auth',
		requiresAuth: true
	},
	contract: {
		name: '授权页面',
		path: '/pages/my-loan/contract',
		requiresAuth: true
	},
	onepage: {
		name: '单页面',
		path: '/pages/onepage/index',
		requiresAuth: false
	},
	my_bill: {
		name: '我的账单',
		path: '/pages/my-bill/index',
		requiresAuth: true
	},
	signature: {
		name: '签约',
		path: '/pages/auth/signature',
		requiresAuth: true
	},
	xieyi: {
		name: '协议',
		path: '/pages/auth/xieyi',
		requiresAuth: true
	},
	goods: {
		name: '产品详情',
		path: '/pages/goods/goods',
		requiresAuth: true
	},
	shiming: {
		name: '实名认证',
		path: '/pages/auth/shiming/shiming',
		requiresAuth: true
	},
	base: {
		name: '实名认证',
		path: '/pages/auth/base/base',
		requiresAuth: true
	},
	contacts: {
		name: '实名认证',
		path: '/pages/auth/base/contacts',
		requiresAuth: true
	},
	protocol:{
		name: '协议',
		path: '/pages/protocol/protocol',
		requiresAuth: true
	},
	signup:{
		name: '签约',
		path: '/pages/signup/signup',
		requiresAuth: true
	},
	receive:{
		name: '选择收款账户',
		path: '/pages/receive/receive',
		requiresAuth: true
	},
	bank:{
		name: '添加银行卡',
		path: '/pages/bank/bank',
		requiresAuth: true
	},
	alipay:{
		name: '绑定支付宝',
		path: '/pages/bank/alipay/index',
		requiresAuth: true
	},
	wxpay:{
		name: '绑定微信',
		path: '/pages/bank/wxpay/index',
		requiresAuth: true
	},
	signup_result:{
		name: '签约',
		path: '/pages/signup-result/index',
		requiresAuth: true
	},
	progress:{
		name: '申请进度',
		path: '/pages/progress/index',
		requiresAuth: true
	},
	bank_index:{
		name: '我的收款账户',
		path: '/pages/bank/index',
		requiresAuth: true
	},
	progressDetail:{
		name: '申请进度',
		path: '/pages/progress/detail',
		requiresAuth: true
	},
	repayment:{
		name: '还款',
		path: '/pages/repayment/index',
		requiresAuth: true
	},
	zhanqishenqing:{
		name: '展期申请',
		path: '/pages/repayment/zhanqishenqing',
		requiresAuth: true
	},
	me:{
		name: '我的',
		path: '/pages/me/index',
		requiresAuth: true
	},
	message:{
		name: '消息',
		path: '/pages/message/index',
		requiresAuth: true
	},
	account_set:{
		name: '账户设置',
		path: '/pages/account-set/index',
		requiresAuth: true
	},
	account_set_tel:{
		name: '更换手机号',
		path: '/pages/account-set/tel',
		requiresAuth: true
	},
	my_loan:{
		name: '我的信贷',
		path: '/pages/my-loan/index',
		requiresAuth: true
	},
	my_loan_detail:{
		name: '借款记录详情',
		path: '/pages/my-loan/detail',
		requiresAuth: true
	},
	help:{
		name: '帮助中心',
		path: '/pages/me/help',
		requiresAuth: true
	},
	help_form:{
		name: '帮助中心',
		path: '/pages/me/help-form',
		requiresAuth: true
	},
	about_us:{
		name: '关于我们',
		path: '/pages/me/about-us',
		requiresAuth: true
	},

	// 非权限路由
	main: {
		name: '引导页',
		path: '/pages/index/main'
	},
	index: {
		name: '首页',
		path: '/pages/index/index'
	},
	category: {
		name: '分类',
		path: '/pages/category/category'
	},
	cart: {
		name: '购物车',
		path: '/pages/cart/cart'
	},
	login: {
		name: '登录',
		path: '/pages/public/login'
	},
	register: {
		name: '注册',
		path: '/pages/public/register'
	},
	loginType: {
		name: '登录类型',
		path: '/pages/public/logintype'
	}
};
