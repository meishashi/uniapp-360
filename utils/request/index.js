import { fetch } from "./request.js"       //引入封装的请求
import indexConfig from '@/config/index.config';

const api = {

	//产品分类
	goodscate: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/goodscate' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 广播
	announceList: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/article?cate_id=5' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 帮助中心
	helpList: function (params) {
		return fetch({
			url : indexConfig.baseUrl + '/api/article?cate_id=7' ,
			data : params ,
			method : 'GET' ,
			load : 0 ,
			auth : false
		})
	},
	// 文章详情
	getArticleData: function (articleId,params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/article/'+articleId ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : false
	  })
	},
	// onepage
	getOnepageData: function (onepageId,params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/onepage/'+onepageId ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 商品列表
	getGoodsList: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/goods' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 商品列表
	postGoodsInfo: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/goodsinfo' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 登录
	login: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/mobilelogin' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 提交订单
	submitOrder: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/order' ,
	    data : params ,
	    method : 'PUT' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 签约提交
	signUpOrder: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/order' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 编辑银行卡
	editMemberBank: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberbank' ,
	    data : params ,
	    method : 'PUT' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 上传图片 -- 手绘base64文件
	uploadBase64: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/uploads_base64' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 取消签约
	cancelOrder: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/order' ,
	    data : params ,
	    method : 'DELETE' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 获取订单列表
	getOrderList: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/order' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 获取订单合同
	getOrderContract: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/order_contract' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 订单详情
	getOrderDetail: function (orderId,params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/order/'+orderId ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 提交反馈
	putFeedBack: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/feedback' ,
	    data : params ,
	    method : 'PUT' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 获取银行列表
	getBankList: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberbank' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 还款明细
	getLoan: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/loan' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 获取未登录信息
	getMessageList: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/message',
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 获取已登录信息
	getMemberMessageList: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/messageindex_m',
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 编辑贷款明细
	postLoan: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/loan' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 按月获取还款日历
	getLoanMonth: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/loanmonth' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 删除银行
	deleteBank: function (bankId,params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberbank/'+bankId ,
	    data : params ,
	    method : 'DELETE' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 添加银行
	addBank: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberbank' ,
	    data : params ,
	    method : 'PUT' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 编辑银行
	editBank: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberbank' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 获取单个银行
	getBank: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberbank' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},


	// 获取会员资料
	getMember: function (member_id,params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/member/'+member_id ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 获取实名认证
	getMemberidcard: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberidcard' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 编辑实名认证
	editMemberidcard: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberidcard' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 获取基本信息
	getMemberInfo: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberinfo' ,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 保存基本信息
	editMemberInfo: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/memberinfo' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 商品图片
	uploadImage: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/uploads' ,
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 获取商品单条数据
	getGoodsDetail: function (goods_id,params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/goods/'+ goods_id,
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 修改手机号
	postChangeMobile: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/changemobile',
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 修改头像
	postMemberAvatar: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/member',
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : true
	  })
	},
	// 获取验证码
	postSmsCode: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/sms',
	    data : params ,
	    method : 'POST' ,
	    load : 0 ,
			auth : false
	  })
	},
	// 获取支付配置
	getSystemset: function (params) {
	  return fetch({
	    url : indexConfig.baseUrl + '/api/systemset',
	    data : params ,
	    method : 'GET' ,
	    load : 0 ,
			auth : true
	  })
	},


}

export default api
