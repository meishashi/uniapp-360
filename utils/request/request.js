// Promise封装请求   options是一个对象里面的属性有url, data, method, load
//url 对应请求路径  data对象请求的参数  method对应请求方法  load对应是否显示加载中 1显示 0不显示
function fetch(options) {
	//加载效果
	if (options.load == 1) {
		uni.showLoading({
			mask: true,
			title: '加载中'
		})
	}

	//是否需要登录
	if (options.auth == 1) {
		let token = uni.getStorageSync('token')
		if (!token) {
			uni.navigateTo({
				url: '/pages/login/index'
			})
			return
		}
	}

	return new Promise((resolve, reject) => {

		// console.log(options.data)
		// console.log(options.url)
		// console.log( uni.getStorageSync('token') )

		uni.request({
			url: options.url,
			data: options.data,
			header: {
				"content-type": "application/x-www-form-urlencoded;charset=utf-8",
				"token": uni.getStorageSync('token'),
			},

			// header: {
			//   "application/x-www-form-urlencoded;charset=utf-8"
			// },

			method: options.method,
			success: function(res) {
				// console.log(res)
				if (res.statusCode != 200) {
					console.log('接口通讯错误！')
				}

				options.load == 1 ? uni.hideLoading() : '';
				//如果没有登录的操作

				if (res.data.code == 0) {
					uni.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 2000
					})
					console.log(res.data)
					return;
				}

				resolve(res.data); //把请求到的数据发到引用请求的地方
			},
			fail: function(err) {
				reject(err)
				options.load == 1 ? uni.hideLoading() : ''
				uni.showToast({
					title: "网络连接超时",
					icon: 'none',
					duration: 3000,
				})
			}
		})
	});

}


module.exports = {
	fetch
}
